Das Team zur Entwicklung des Programms "Sudoku Solver" besteht aus:
-Felix Wiedermann
-Vanessa Mielke

Die Aufgabenverteilung:
Gemeinsam:
Theoretische Aufstellung der Regeln eines Sudokus und Einigung auf Art der Realisierung.

Felix Wiedermann:
Regelrealisierung

Vanessa Mielke:
Sudokus einschreiben

Gemeinsam:
Zusammenfügung und Besprechung der Einzelteile

Vanessa Mielke:
Realisierung der "Probierfunktion"

Felix Wiedermann:
Realisierung der Endprüfung