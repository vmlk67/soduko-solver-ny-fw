Unser Projekt heißt "Sudoku Solver".
Das Programm soll eins von 3 vorgebenen Sudokus durch Zufall aussuchen.
Beim Loesen setzt es nur Werte ein, wenn es nur eine Moeglichkeit für ein Feld gibt.
Bei mehreren Möglichkeiten soll es zum nächsten Feld weitergehen und wieder die Anzahl der Moeglichkeiten prüfen.

Die vermutliche groesste technische Herausforderung wird sein:

- Verallgemeinerung der Regeln eines Sudokus