#include "stdafx.h"

int Sudoku[9][9] = { 0 }; //2-Dimensionales Array um die Tabelle eines Sudokus darzustellen und an jedem Koordinatenpaar eine Zahl abzuspeichern
int x, y; //Koordianten--> h�ufig verwendete Variablen

int Sudoku1[9][9] = {
	{ 0, 7, 0, 0, 3, 0, 6, 0, 8 },
	{ 6, 8, 4, 0, 0, 0, 1, 9, 0 },
	{ 1, 0, 0, 0, 8, 4, 0, 5, 0 },
	{ 5, 6, 7, 9, 0, 0, 0, 8, 0 },
	{ 4, 1, 0, 5, 6, 0, 9, 3, 0 },
	{ 9, 0, 3, 0, 0, 0, 0, 7, 6 },
	{ 8, 0, 0, 7, 0, 6, 0, 2, 5 },
	{ 7, 0, 6, 0, 0, 2, 0, 2, 5 },
	{ 0, 0, 2, 8, 9, 1, 7, 0, 4 }
};

int Sudoku2[9][9] = {
	{ 8, 3, 9, 0, 6, 0, 4, 7, 0 },
	{ 4, 5, 0, 3, 9, 0, 0, 0, 2 },
	{ 0, 2, 0, 5, 7, 4, 8, 0, 0 },
	{ 0, 0, 3, 8, 2, 0, 0, 0, 1 },
	{ 0, 1, 5, 4, 3, 0, 6, 2, 0 },
	{ 0, 0, 0, 1, 0, 7, 0, 3, 4 },
	{ 3, 7, 0, 0, 0, 2, 5, 0, 6 },
	{ 5, 0, 1, 7, 0, 3, 2, 0, 9 },
	{ 0, 0, 0, 6, 1, 0, 3, 4, 0 }
};

int Sudoku3[9][9] = {
	{ 0, 0, 4, 8, 6, 7, 0, 9, 0 },
	{ 2, 9, 0, 4, 3, 0, 7, 8, 0 },
	{ 6, 0, 7, 1, 0, 0, 0, 0, 3 },
	{ 9, 1, 0, 0, 4, 0, 6, 7, 8 },
	{ 3, 4, 5, 0, 0, 6, 0, 1, 0 },
	{ 8, 7, 0, 0, 0, 9, 3, 0, 4 },
	{ 0, 2, 9, 3, 5, 0, 0, 0, 0 },
	{ 4, 0, 0, 0, 7, 8, 0, 0, 1 },
	{ 0, 6, 8, 0, 0, 0, 4, 3, 0 },
};

int Sudoku4[9][9] = {
	{ 8, 0, 0, 0, 4, 0, 0, 5, 0 },
	{ 0, 2, 1, 8, 0, 0, 0, 3, 4 },
	{ 0, 0, 0, 0, 9, 5, 0, 0, 8 },
	{ 0, 0, 5, 0, 2, 7, 0, 0, 1 },
	{ 9, 8, 0, 0, 0, 0, 0, 7, 2 },
	{ 7, 0, 0, 6, 3, 0, 5, 0, 0 },
	{ 6, 0, 0, 1, 8, 0, 0, 0, 0 },
	{ 1, 7, 0, 0, 0, 3, 8, 2, 0 },
	{ 0, 5, 0, 0, 6, 0, 0, 0, 3 }
};

void design() { //Aufbau und Aussehen des Sudokus
	groesse(9, 9);
	flaeche(WHITE);
	rahmen(WHITE);
}

void Sudokubereitstellen() { //Per rand-Funktion wird ein Sudoku zuf�llig ausgesucht und in Sudoku[][] abgespeichert
	switch (rand() % 4 + 1) {
	case 1:
		for (x = 0; x < 9; x++) {
			for (y = 0; y <9; y++) {
				Sudoku[y][x] = Sudoku1[y][x];
			}
		}
		break;
	case 2:
		for (x = 0; x < 9; x++) {
			for (y = 0; y <9; y++) {
				Sudoku[y][x] = Sudoku2[y][x];
			}
		}
		break;
	case 3:
		for (x = 0; x < 9; x++) {
			for (y = 0; y < 9; y++) {
				Sudoku[y][x] = Sudoku3[y][x];
			}
		}
		break;
	case 4:
		for (x = 0; x < 9; x++) {
			for (y = 0; y < 9; y++) {
				Sudoku[y][x] = Sudoku4[y][x];
			}
		}
		break;
	}
}

void eintragen(int x, int y, int i) { //Ziffer i wird im Sudoku[y][x] abgespeichert und via Umformung in char in BoS angezeigt
	char z;
	Sudoku[y][x] = i;
	z = i + 48;
	zeichen2(x, y, z);
}

void ggWerteanzeigen() {
	int feldx = 0, feldy = 0;
	char z;
	for (x = 0; x < 9; x++) {
		for (y = 0; y < 9; y++) { //Das Sudoku wird in 3x3 Quadrate unterteilt, diese werden abwechselnd eingef�rbt
			feldx = (int)x / 3 * 3;
			feldy = (int)y / 3 * 3;
			if (feldx % 2 == feldy % 2) {
				farbe2(x, y, LIGHTSKYBLUE);
			}
			if (Sudoku[y][x] != 0) { //Vorgegebene Felder werden mit den Zahlen besetzt und im selben Moment mit dem Radius 0.4 verkleinert
				z = Sudoku[y][x] + 48;
				zeichen2(x, y, z);
				symbolGroesse2(x, y, 0.4);
			}
		}
	}
}

int zifferschoninZeile(int y, int r) { //Die Funktion pr�ft, ob die Zahl r sich schon in der Zeile befindet (x-Komponente)
	int i = 0;
	for (i = 0; i < 9; i++) {
		if (Sudoku[y][i] == r)
			return 1;
	}
	return 0;
}

int zifferschoninSpalte(int x, int r) { //Funktion pr�ft ob Ziffer r bereits in der Spalte ist (y-Komponente)
	int i = 0;
	for (i = 0; i < 9; i++) {
		if (Sudoku[i][x] == r)
			return 1;
	}
	return 0;
}

int zifferschoninFeld(int x, int y, int r) { //Funktion pr�ft ob die Ziffer r im Feld vorhanden ist
	int feldy = 0, feldx = 0, i = 0, j = 0;
	feldx = (int)x / 3 * 3;
	feldy = (int)y / 3 * 3;
	for (i = feldx; i < feldx + 3; i++) {
		for (j = feldy; j < feldy + 3; j++) {
			if (Sudoku[j][i] == r)
				return 1;
		}
	}
	return 0;
}

int zifferzulaessig(int x, int y, int r) { //pr�ft ob eine Ziffer eingesetzt werden k�nnte, daf�r darf sie weder in der Zeile, Spalte noch im Feld schon vorhanden sein
	if (Sudoku[y][x] != 0)
		return 0;
	if (zifferschoninSpalte(x, r) == 0 && zifferschoninZeile(y, r) == 0 && zifferschoninFeld(x, y, r) == 0)
		return 1;
	return 0;
}

void WertinleereZelle(int x, int y) { //Die M�glichkeiten f�r ein leeres Feld werden gez�hlt
	int i = 0, feldx = 0, feldy = 0, k = 0, j = 0, p = 0, d = 0, e = 0;
	char z;
	if (Sudoku[y][x] == 0) {
		for (i = 1; i < 10; i++) {
			if (zifferzulaessig(x, y, i) == 1) {
				feldx = (int)x / 3 * 3;
				feldy = (int)y / 3 * 3;
				for (k = feldx; k < feldx + 3; k++) {
					for (j = feldy; j < feldy + 3; j++) {
						if (zifferzulaessig(k, j, i) == 1) {
							p++;
						}
					}
				}
				if (p == 1) { //M�glichkeit f�r ein 3x3 Quadrat
					eintragen(x, y, i);
					d = 0;
					break;
				}
				d++; //Die M�glichkeiten
				e = i;
			}
		}
		if (d == 1) {
			eintragen(x, y, e);
		}
		d = 0;
	}
}

void main() { //Alle Funktionen arbeiten zusammen und l�sen so das Sudoku nacheinander und es wird gepr�ft, ob das Sudoku vollst�ndig ausgef�llt wurde
			  //Deklarationen
	srand(time(NULL));

	int ausgef�llt = 0, done = 0;

	design();
	Sudokubereitstellen();
	ggWerteanzeigen();

	while (!ausgef�llt) {
		done = 0;

		for (x = 0; x < 9; x++) {
			for (y = 0; y < 9; y++) {
				WertinleereZelle(x, y);
			}
		}

		for (x = 0; x < 9; x++) {
			if (done)
				break;
			for (y = 0; y < 9; y++) {
				if (Sudoku[y][x] == 0) {
					ausgef�llt = 0;
					done = 1;
					break;
				}
				else {
					ausgef�llt = 1;
				}
			}
		}
	}
}